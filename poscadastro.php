<?php
include("manifestacao.class.php");
include("manifestacaodao.class.php");


//recebendo dos dados do formulario
$dados_form = $_POST;

//teste com o post 
/*foreach($dados_form as $chave => $dado){
  echo $chave." = ".$dado."<br/>";
}
echo "<br/>"."<br/>";*/

//instanciando objeto manifestacao
//cujo os atributos receberao os
//dados do form(padrao pojo do java)
$manifestacao = new Manifestacao();

//attribuindo os elementos do array
//de dados do form aos attributos
//do objeto manifestacao manualmente
$manifestacao->setProtocolo(intval(""));//proto vazio usar cod auto_inc mysql
$manifestacao->setNome($dados_form['nome']);
$manifestacao->setEmail($dados_form['email']);
$manifestacao->setCpf_cnpj($dados_form['cpf_cnpj']);
$manifestacao->setRg($dados_form['rg']);
$manifestacao->setCep($dados_form['cep']);
$manifestacao->setLogradouro($dados_form['logradouro']);
$manifestacao->setNumero($dados_form['numero']);
$manifestacao->setComplemento($dados_form['complemento']);
$manifestacao->setBairro($dados_form['bairro']);
$manifestacao->setCidade($dados_form['cidade']);
$manifestacao->setUf($dados_form['uf']);//n obrgt. sem vrfc do campo select
$manifestacao->setPais($dados_form['pais']);
$manifestacao->setDdd($dados_form['ddd']);
$manifestacao->setTelefone($dados_form['telefone']);
$manifestacao->setDdd_cel($dados_form['ddd_cel']);
$manifestacao->setCelular($dados_form['celular']);
$manifestacao->setDescricao($dados_form['descricao']);
$manifestacao->setData(date('Y/m/d H:i',time()));

 
 
//teste com attributos do objeto manifestacao
/*echo $manifestacao->getProtocolo()."<br/>";
echo $manifestacao->getNome()."<br/>";
echo $manifestacao->getEmail()."<br/>";
echo $manifestacao->getCpf_cnpj()."<br/>";
echo $manifestacao->getRg()."<br/>";
echo $manifestacao->getCep()."<br/>";
echo $manifestacao->getLogradouro()."<br/>";
echo $manifestacao->getNumero()."<br/>";
echo $manifestacao->getComplemento()."<br/>";
echo $manifestacao->getBairro()."<br/>";
echo $manifestacao->getCidade()."<br/>";
echo $manifestacao->getUf()."<br/>";
echo $manifestacao->getPais()."<br/>";
echo $manifestacao->getDdd()."<br/>";
echo $manifestacao->getTelefone($dados_form['telefone'])."<br/>";
echo $manifestacao->getDdd_cel()."<br/>";
echo $manifestacao->getCelular()."<br/>";
echo $manifestacao->getDescricao()."<br/>";
echo $manifestacao->getData()."<br/>"; 
echo date_default_timezone_get();*/

//instancia Classe DAO responsavel
//por conectar-se ao bd e salvar 
//dados da manifestacao
$manifestacaoDAO = new ManifestacaoDAO();
//salva objeto no banco de dados
$manifestacaoDAO->Salvar($manifestacao);


?>
