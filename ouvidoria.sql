-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 01, 2018 at 03:32 PM
-- Server version: 5.7.23-0ubuntu0.18.04.1
-- PHP Version: 7.2.7-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ouvidoria`
--

-- --------------------------------------------------------

--
-- Table structure for table `mapa`
--

CREATE TABLE `mapa` (
  `protocolo` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cpf_cnpj` varchar(20) NOT NULL,
  `rg` varchar(20) NOT NULL,
  `cep` varchar(20) NOT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(11) NOT NULL,
  `complemento` varchar(30) NOT NULL,
  `bairro` varchar(50) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `uf` varchar(3) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `ddd` varchar(5) NOT NULL,
  `telefone` varchar(30) NOT NULL,
  `ddd_cel` varchar(5) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `descricao` text NOT NULL,
  `data_manifestacao` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapa`
--

INSERT INTO `mapa` (`protocolo`, `nome`, `email`, `cpf_cnpj`, `rg`, `cep`, `logradouro`, `numero`, `complemento`, `bairro`, `cidade`, `uf`, `pais`, `ddd`, `telefone`, `ddd_cel`, `celular`, `descricao`, `data_manifestacao`) VALUES
(1, 'João Felicio Neto', 'jf@email.com', '286447758-92', '37214296-X', '4509700', 'Av: Gilbérica Rodrigues Bourbom Jr.', '120', 'Casa', 'Floral', 'São Joaquim das Palmeiras', 'MG', 'Brasil', '123', '19823-0988', '321', '23456-78', 'Isto é apenas um exemplo de descrição de manifestação                ', '2018-08-01'),
(2, 'Lindivina Patrícia Felisberto', 'linda@pat.com.pt', '192664027-52', '53921692-8', '54380-111', 'Rua: Sebastião Ricardo', '79', 'Apartamento', 'Juaraquararé de cima', 'Buxelópolis', 'RO', 'Brasil', '876', '9876-5493', '678', '93212-1977', 'Outro Teste                 ', '2018-08-01'),
(3, 'Eduardo Ronivaldison Ramires', 'Edu@email.com', '789653281-75', '25910489-X', '76430-669', 'Praça: Joaquim francisvaldinei filho', '1297', 'Casa', 'Lindenbergno', 'Camaçari', 'BA', 'Brasil', '211', '982819-987', '129', '8192-0987', '  Isto é um exemplo de Descrição de Manifestação              ', '2018-08-01'),
(4, 'Ricardo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Exemplo de Descrição                ', '2018-08-01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mapa`
--
ALTER TABLE `mapa`
  ADD PRIMARY KEY (`protocolo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mapa`
--
ALTER TABLE `mapa`
  MODIFY `protocolo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
