<?php

class Manifestacao{
    
    private $protocolo;
    private $nome;
    private $email;
    private $cpf_cnpj;
    private $rg;
    private $cep;
    private $logradouro;
    private $numero;
    private $complemento;
    private $bairro;
    private $cidade;
    private $uf;
    private $pais;
    private $ddd;
    private $telefone;
    private $ddd_cel;
    private $celular;
    private $descricao;
    private $data;
    
    
    public function getProtocolo() {
        return $this->protocolo;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getCpf_cnpj() {
        return $this->cpf_cnpj;
    }

    public function getRg() {
        return $this->rg;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getLogradouro() {
        return $this->logradouro;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getComplemento() {
        return $this->complemento;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getUf() {
        return $this->uf;
    }

    public function getPais() {
        return $this->pais;
    }

    public function getDdd() {
        return $this->ddd;
    }

    public function getTelefone() {
        return $this->telefone;
    }

    public function getDdd_cel() {
        return $this->ddd_cel;
    }

    public function getCelular() {
        return $this->celular;
    }

    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getData(){
        return $this->data;
    }

    public function setProtocolo($protocolo) {
        $this->protocolo = $protocolo;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setCpf_cnpj($cpf_cnpj) {
        $this->cpf_cnpj = $cpf_cnpj;
    }

    public function setRg($rg) {
        $this->rg = $rg;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setLogradouro($logradouro) {
        $this->logradouro = $logradouro;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setUf($uf) {
        $this->uf = $uf;
    }

    public function setPais($pais) {
        $this->pais = $pais;
    }

    public function setDdd($ddd) {
        $this->ddd = $ddd;
    }

    public function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    public function setDdd_cel($ddd_cel) {
        $this->ddd_cel = $ddd_cel;
    }

    public function setCelular($celular) {
        $this->celular = $celular;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setData($data){
        $this->data = $data;
    }
    
}

?>
