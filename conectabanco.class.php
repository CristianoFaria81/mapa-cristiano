<?php
include("auxiliaconexaobd.php");

//usando o  padrao singleton
class ConectaBanco{
    private static $instancia;
              
    
    //construtor da classe vazio
    //privado para impedir instanciacao
    //da classe
    private function _construct(){}
    
    //funcao de clonagem de objeto
    //vazia para impedir a copia do objeto
    private function _clone(){}
    
    
    //funcao que retorna apenas uma conexao com bd
    public static function getConexao(){
        if(!isset(self::$instancia)){
            try{
              self::$instancia = new PDO(FONTE_DADOS,BANCO_USUARIO,BANCO_SENHA);
              self::$instancia->setAttribute(PDO::ATTR_ERRMODE
                      ,PDO::ERRMODE_EXCEPTION);
            }catch(PDOException $e){
              echo "falha na conexão com banco de dados ".$e->getMessage();
              die();
            }
        }    
        
        return self::$instancia;
    }
    
    

    
}



?>
