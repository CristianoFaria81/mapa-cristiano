<?php
include 'conectabanco.class.php';


    class ManifestacaoDAO{
       private $conexao_bd = null;
       private $Dados;
       private $insercao;
       private $busca;
       private $Resultado_Consulta;
       private $protocolo;
       
       
     
       
       public function Salvar(Manifestacao $Manifestacao){
           try{ 
               $this->Dados = array(
                "protocolo"=>null,
                "nome"=>strip_tags($Manifestacao->getNome()),
                "email"=>strip_tags($Manifestacao->getEmail()),
                "cpf_cnpj"=>strip_tags($Manifestacao->getCpf_cnpj()),
                "rg"=>strip_tags($Manifestacao->getRg()),
                "cep"=>strip_tags($Manifestacao->getCep()),
                "logradouro"=>strip_tags($Manifestacao->getLogradouro()),
                "numero"=>strip_tags($Manifestacao->getNumero()),
                "complemento"=>strip_tags($Manifestacao->getComplemento()),
                "bairro"=>strip_tags($Manifestacao->getBairro()),
                "cidade"=>strip_tags($Manifestacao->getCidade()),
                "uf"=>strip_tags($Manifestacao->getUF()),
                "pais"=>strip_tags($Manifestacao->getPais()),
                "ddd"=>strip_tags($Manifestacao->getDdd()),
                "telefone"=>strip_tags($Manifestacao->getTelefone()),
                "ddd_cel"=>strip_tags($Manifestacao->getDdd_cel()),
                "celular"=>strip_tags($Manifestacao->getCelular()),
                "descricao"=>strip_tags($Manifestacao->getDescricao()),
                "data_manifestacao"=>($Manifestacao->getData()),
            );
            
              $this->conexao_bd = ConectaBanco::getConexao();
              $sql = "Insert into mapa(protocolo,nome,email,cpf_cnpj,rg,cep,"
                      . "logradouro,numero,complemento,bairro,cidade,uf,pais,"
                      . "ddd,telefone,ddd_cel,celular,descricao"
                      . ",data_manifestacao)"
                      . "Values(:protocolo, :nome, :email, :cpf_cnpj, :rg,:cep,"
                      . ":logradouro, :numero, :complemento, :bairro, :cidade ,"
                      . ":uf, :pais, :ddd, :telefone, :ddd_cel, :celular, "
                      . ":descricao,:data_manifestacao)";
              //print_r($this->Dados); //teste com o array
              $this->insercao = $this->conexao_bd->prepare($sql);
              //$this->insercao->debugDumpParams(); teste de parametros
              $this->insercao->execute($this->Dados);
              header('location:index.html?situacao=1');
             }catch(PDOException $ex){
              die("Erro ao conectar com Banco de Dados ".$ex->getMessage());
            }
       }
       
       public function Pesquisar(int $p){
           $this->protocolo=$p;
           $sql = "Select protocolo,nome,email,cpf_cnpj,rg,cep,logradouro,"
                   . "numero,complemento,bairro,cidade,uf,pais,ddd,telefone,"
                   . "ddd_cel,celular,descricao,data_manifestacao From mapa"
                   . " Where protocolo = :protocolo";
           $this->conexao_bd = ConectaBanco::getConexao();
           $this->busca= $this->conexao_bd->prepare($sql);
           //$this->busca->debugDumpParams();//teste de sql
           $this->busca->execute(array(':protocolo'=> $this->protocolo));
           $this->Resultado_Consulta = $this->busca->fetchAll();
           //var_dump($this->Resultado_Consulta);teste de retorno consulta
           if(!empty($this->Resultado_Consulta)){
              //var_dump($this->Resultado_Consulta); 
              return $this->Resultado_Consulta;
              /*foreach($this->Resultado_Consulta as $valor => $chave){
                  echo $chave[1];
              }*/
           }else{
               header('location:index.html?situacao=2');
           }
       }
       
       
       
    }

?>
