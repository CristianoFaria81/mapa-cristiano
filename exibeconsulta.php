<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,
     shrink-to-fit=no">

    <title>Ouvidoria UniCesumar</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript" src="dist/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="dist/bootstrap/js/bootstrap.min.js">
    </script>
  </head>

  <body>
      <?php
        include 'manifestacaodao.class.php';
    
        if(isset($_POST['protocolo'])){
            $id = intval($_POST['protocolo']);
            $manifestacaoDAO = new ManifestacaoDAO();
            $res = $manifestacaoDAO->Pesquisar($id);//recebe array
            //var_dump($res); //teste array
            $dados=array(); //cria novo array
            foreach($res as $item => $dados){
                //criado apenas para
                //iterar sobre o array $res
                if($item>18){break;}
            }
        }    
        
      ?>
    <div class="container">
      <div class="text-center" style="margin-top: 20px">
        <img src="unicesumar.png" />

        <h2 style="margin-top:20px">Ouvidoria Consulta</h2>
        <p class="lead">Caso necessário entre em contato conosco.<br />
          Estamos à disposição para mais esclarecimentos sobre a utilização
          do serviço.</p>
        <h2> Nº de Protocolo da Ocorrência: <?php echo $dados[0];?></h2>
      </div>

      <div class="row">

        <div class="col-md-12">
          <form action="poscadastro.php" method="post">
            <h4>Dados Pessoais</h4>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="nome">Nome (*)</label>
                <input type="text" class="form-control" id="nome" name="nome"
                       required value="<?php echo $dados[1];?>" readonly="true"
                >
              </div>

                <div class="col-md-6">
                    <label for="username">Email</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="email" class="form-control" id="email"
                        name="email"value="<?php echo $dados[2];?>"
                        readonly="true">
                    </div>
                </div>

              <div class="col-md-6 mb-3">
                <label for="cpf_cnpj">CPF/CNPJ</label>
                <input type="text" class="form-control" id="cpf_cnpj"
                name="cpf_cnpj" value="<?php echo $dados[3];?>"
                readonly="true">
              </div>
              <div class="col-md-6 mb-3">
                <label for="rg">RG</label>
                <input type="text" class="form-control" id="rg"
                name="rg" value="<?php echo $dados[4];?>"
                readonly="true">
              </div>
            </div>

            <h4>Dados Residenciais</h4>

            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" id="cep"
                name="cep" value="<?php echo $dados[5];?>" readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                <label for="logradouro">Logradouro</label>
                <input type="text" class="form-control" id="logradouro"
                name="logradouro" value="<?php echo $dados[6];?>" 
                readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                <label for="numero">Número</label>
                <input type="text" class="form-control" id="numero"
                name="numero" value="<?php echo $dados[7];?>" 
                readonly="true">
              </div>
            </div>

            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="complemento">Complemento</label>
                <input type="text" class="form-control" id="complemento"
                name="complemento" value="<?php echo $dados[8];?>"
                readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" id="bairro"
                name="bairro" value="<?php echo $dados[9];?>"
                readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade"
                name="cidade" value="<?php echo $dados[10];?>" 
                readonly="true">
              </div>
            </div>

            <div class="row">
              <div class="col-md-4 mb-3">
                <label for="uf">UF</label>
                <input type="text" class="form-control" id="uf"
                name="uf" value="<?php echo $dados[11];?>" readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                <label for="pais">País</label>
                <input type="text" class="form-control" id="pais"
                name="pais" value="<?php echo $dados[12];?>"
                readonly="true">
              </div>
              <div class="col-md-4 mb-3">
                  <div class="row">
                      <div class="col-md-4 mb-3">
                        <label for="ddd">DDD</label>
                        <input type="text" class="form-control" id="ddd"
                        name="ddd" size="5" value="<?php echo $dados[13];?>"
                        readonly="true">
                      </div>
                      <div class="col-md-8 mb-3">
                        <label for="telefone">Telefone</label>
                        <input type="text" class="form-control" id="telefone"
                        name="telefone" size="15" 
                        value="<?php echo $dados[14];?>" readonly="true">
                      </div>
                  </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4 mb-2">
                  <div class="row">
                      <div class="col-md-4 mb-3">
                        <label for="ddd_cel">DDD</label>
                        <input type="text" class="form-control" id="ddd_cel"
                        name="ddd_cel" size="5" value="<?php echo $dados[15];?>"
                        readonly="true">
                      </div>
                      <div class="col-md-8 mb-3">
                        <label for="celular">Celular</label>
                        <input type="text" class="form-control" id="celular"
                        name="celular" size="15" 
                        value="<?php echo $dados[16];?>" readonly="true">
                      </div>
                      <div class="col-md-5 mb-3">
                        <label for="Data">Data da Manifestação</label>
                        <input type="text" class="form-control" id="data"
                        size="15" value="<?php echo date('d/m/Y',
                                    strtotime($dados[18])
                                );?>"
                        readonly="true">
                      </div>
                  </div>         
              </div>
            </div>

            <h4>Dados Residenciais</h4>
            <div class="row">
              <div class="col-md-12">
                <label for="descricao">Descrição (*)</label>
                <textarea rows="12" class="form-control" id="descricao"
                name="descricao" readonly="true">
                    <?php echo $dados[17];?>
                </textarea>
              </div>
            </div>

            <hr />

            <!--<input type="submit" class="btn btn-primary btn-lg btn-block"
            type="submit" value="Enviar" style="margin-bottom:50px">!-->
            <a href="index.html" class="btn btn-primary btn-lg btn-block">
                Voltar
            </a><br/><br/>
            
          </form>
        </div>

      </div>

    </div><!-- container -->
  </body>
</html>